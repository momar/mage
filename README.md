# mage

mage magically manages the public keys of a team when working with age-encrypted files.

- Always ensures that encrypted *.age files are using the correct set of public keys.
- The private key is stored in a file called `~/.mage-key`, unless another one is specified with `-i`.
- Public keys to use for a specific file are specified in ".mage-publickeys", in one of two ways:
  - a plain-text list of public keys
  - a shell script (starting with a shebang) that returns a list of public keys to use for the file (`$1`)

```
mage [-i KEY] cat FILE
  Decrypt an encrypted file to standard output.

mage [-i KEY] edit [--manual] FILE
  Edit an encrypted file and re-encrypt it again.
  If --manual is set, don't start \$EDITOR but rather wait for the user to press return - useful for e.g. IDEs.

mage [-i KEY] encrypt INPUT-FILE [OUTPUT-FILE]
  Encrypt a file & remove the input - mostly useful after "mage edit" errors, or with "mage expose --writable".
  OUTPUT-FILE defaults to INPUT-FILE.age

mage [-i KEY] fix [FILE...]
  Re-encrypt all decryptable files recursively.

mage [-i KEY] expose [--writable] [FILE...]
  Decrypt all decryptable files recursively (read-only unless --writable is specified).
  For all FILE.age files, the corresponding FILE (without the .age extension) will be CREATED OR OVERWRITTEN.
  Should only be used for automated processing. After the files have been processed, run "mage unexpose".

mage [-i KEY] unexpose [--write-all] [FILE...]
  Recursively remove all files FILE if FILE.age exists as well.
  CHANGES ARE IGNORED, you need to manually encrypt files before!
```

Roadmap until it's usable:
- [ ] Implement expose & unexpose
- [ ] Cache passphrase
- [ ] Test with Ansible (when to enter passphrase?)
- [ ] Test all other examples
- [ ] Write unit tests
- [ ] Release v1.0
