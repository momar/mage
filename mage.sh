#!/bin/sh
set -eu

# Check private key & generate one if there is none
PRIVATE_KEY="$HOME/.mage-key"
if [ $# -ge 2 ] && [ "$1" = "-i" ]; then PRIVATE_KEY="$2"; shift 2; fi
if ! [ -e "${PRIVATE_KEY}" ]; then
  echo "Private key '${PRIVATE_KEY}' doesn't exist, creating one..."
  age-keygen | age -p > "${PRIVATE_KEY}"
fi

# Get recipients for a file
mage_recipients() {
  FILEPATH=$(realpath -Pe "$1")
  cd "$(dirname "${FILEPATH}")"

  # Get directory with ".mage-keys" file
  FILESYSTEM=$(stat -c "%d" .)
  while ! [ -e ".mage-publickeys" ]; do
    if [ "$(pwd)" = "/" ] || [ "$(stat -c "%d" ..)" != "${FILESYSTEM}" ]; then
      echo "No .mage-publickeys file found in parent directories of the same file system as the file." >&2
      exit 1
    fi
    cd ..
  done

  # Get the public keys from the file, either as a script with a shebang or directly as a text file
  if [ "$(head -c2 .mage-publickeys)" = "#!" ]; then
    RECIPIENTS=$(./.mage-publickeys "$(realpath "$FILEPATH" --relative-to .)")
  else
    RECIPIENTS=$(sed -e '/^\s*#|^\s*$/d' -e 's/#.*//' .mage-publickeys)
  fi
  mage_validate_recipients "${RECIPIENTS}"
  echo "${RECIPIENTS}"
}

# Validate that the recipients aren't empty, are in the correct format & contain the own private key
mage_validate_recipients() {
  : # TODO: implement
}

case "${1:-}" in
  "cat")
    shift
    if [ $# -ne 1 ]; then echo "'mage cat' requires exactly one argument." >&2; exit 1; fi
    # Just print the decrypted file contents
    age --decrypt --identity "${PRIVATE_KEY}" --output - "$1"
    ;;

  "encrypt")
    shift
    if [ $# -ne 2 ]; then echo "'mage encrypt' requires exactly two arguments." >&2; exit 1; fi

    # Create a temporary file to store the recipients in (so that stdin can be used as an input)
    RECIPIENTS_FILE=$(mktemp --tmpdir mage-recipients.XXXXXXXXXX)
    mage_cleanup() { rm -f "${RECIPIENTS_FILE}"; }
    trap mage_cleanup EXIT

    # Encrypt the file
    mage_recipients "$2" > "${RECIPIENTS_FILE}"
    age --encrypt --recipients-file "${RECIPIENTS_FILE}" --armor --output "$2" "$1"
    ;;

  "edit")
    shift
    MAGE_MANUAL=0
    if [ $# -ge 1 ] && [ "$1" = "--manual" ]; then MAGE_MANUAL=1; shift; fi
    if [ $# -ne 1 ]; then echo "'mage edit' requires exactly one argument." >&2; exit 1; fi
    case "$1" in
      *.age) ;;
      *) echo "File is not a *.age file: '$1'" >&2; exit 1 ;;
    esac

    # Get & validate recipients before decryption to show errors early
    FILEPATH=$(realpath -Pe "$1")
    RECIPIENTS=$(mage_recipients "$FILEPATH")

    # Create a temporary file that can then be edited
    DECRYPTED_FILE=$(mktemp --tmpdir mage.XXXXXXXXXX)
    KEEP_DECRYPTED_FILE=0

    # Clean up on exit
    mage_cleanup() {
      if [ $KEEP_DECRYPTED_FILE -eq 1 ]; then
        echo "Encryption failed, will keep temporary file ${DECRYPTED_FILE}." >&2
        echo "You can run the following command to try again: $0 encrypt '${DECRYPTED_FILE}' > '$FILEPATH'" >&2
        # TODO: ask, so no unencrypted files are stuck on the system!
      else
        rm -f "${DECRYPTED_FILE}"
      fi
    }
    trap mage_cleanup EXIT

    # Decrypt the file if it exists
    if [ -e "${FILEPATH}" ]; then
      age --decrypt --identity "${PRIVATE_KEY}" --output "${DECRYPTED_FILE}" "${FILEPATH}"
    fi

    # Edit the file
    KEEP_DECRYPTED_FILE=1
    if [ "$MAGE_MANUAL" = "1" ]; then
      echo "You can now edit the decrypted file - press return after you're done to re-encrypt:" >&2
      echo "${DECRYPTED_FILE}"
      read -r REPLY
    else
      ${EDITOR:-nano} "${DECRYPTED_FILE}"
    fi

    # Encrypt the file again
    ENCRYPTION_STATUS=0
    echo "${RECIPIENTS}" | age --encrypt --recipients-file - --armor --output "${FILEPATH}" "${DECRYPTED_FILE}"
    KEEP_DECRYPTED_FILE=0
    ;;

  "fix")
    shift
    if [ $# -gt 0 ]; then
      while [ $# -gt 0 ]; do
        case "$1" in
          *.age) ;;
          *) echo "File is not a *.age file: '$1'" >&2; shift; continue ;;
        esac

        # Decrypt to a variable
        DECRYPTION_STATUS=0
        # TODO: provide a way to cache the passphrase for the private key
        DECRYPTED_CONTENTS=$(age --decrypt --identity "${PRIVATE_KEY}" --output - "$1") || DECRYPTION_STATUS=$?

        if [ $DECRYPTION_STATUS -ne 0 ]; then echo "[NO KEY] $1" >&2
        else
          # Encrypt again to the file
          ENCRYPTION_STATUS=0
          echo "${DECRYPTED_CONTENTS}" | "$0" encrypt - "$1" || ENCRYPTION_STATUS=$?

          if [ $ENCRYPTION_STATUS -ne 0 ]; then echo "[ERROR!] $1" >&2
          else echo "[ OKAY ] $1" >&2
          fi
        fi
        shift
      done
    else
      # Without an argument, fix every *.age file
      find . -name '*.age' -exec "$0" "-i" "${PRIVATE_KEY}" fix "{}" "+"
    fi
    ;;

  *)
    cat <<EOF
mage magically manages the public keys of a team when working with age-encrypted files.
- Always ensures that encrypted *.age files are using the correct set of public keys.
- The private key is stored in a file called "~/.mage-key", unless another one is specified with "-i".
- Public keys to use for a specific file are specified in ".mage-publickeys", in one of two ways:
  - a plain-text list of public keys
  - a shell script (starting with a shebang) that returns a list of public keys to use for the file ("\$1")

mage [-i KEY] cat FILE
  Decrypt an encrypted file to standard output.

mage [-i KEY] edit [--manual] FILE
  Edit an encrypted file and re-encrypt it again.
  If --manual is set, don't start \$EDITOR but rather wait for the user to press return - useful for e.g. IDEs.

mage [-i KEY] encrypt INPUT-FILE OUTPUT-FILE
  Encrypt a file & remove the input - mostly useful after "mage edit" errors, or with "mage expose --writable".

mage [-i KEY] fix [FILE...]
  Re-encrypt all decryptable files recursively.

mage [-i KEY] expose [--writable] [FILE...]
  Decrypt all decryptable files recursively (read-only unless --writable is specified).
  For all FILE.age files, the corresponding FILE (without the .age extension) will be CREATED OR OVERWRITTEN.
  Should only be used for automated processing. After the files have been processed, run "mage unexpose".

mage [-i KEY] unexpose [--write-all] [FILE...]
  Remove all files FILE if FILE.age exists as well.
  CHANGES ARE IGNORED, you need to manually encrypt files before!
EOF
    ;;
esac
